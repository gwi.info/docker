#!/bin/bash

echo 'Run scripts download database...'

if [ ! -f $PWD/install/db/*.sql.gz ]; then
wget --user $PROJECT_BASIC_AUTH_LOGIN --password $PROJECT_BASIC_AUTH_PASSWORD -c $PROJECT_DATABASE_PATH -P $PWD/install/db
gunzip -c $PWD/install/db/$PROJECT_BACKUP_NAME_DB > $PWD/install/db/mysql.backup.sql
fi

echo 'Stop download database...'
